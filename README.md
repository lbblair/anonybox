An anonymous suggestion box.

Create a box by signing up and send the link via email to get anonymous responses.

Still a Work in Progess...

Built with Angular-CLI

Using MEAN stack.

Node version ~ 6.9.1  
NPM version ~ 3.10.8  

To use for development:  

Install Node if not installed  

Install MongoDB  

Check node and npm versions  

Open terminal and type:  
node -v  
npm -v

Install Angular-CLI

Open terminal and type:  
npm i -g angular-cli

Clone project with git or download and unzip to desired directory

Install Node modules while in project root directory  

Open terminal and type:  
npm i

Open terminal and type:  
ng build && node server.js

to run project

Happy Browsing!


#TODO's
... a lot ;)   
...add update account  
...add all styles once main functionality is working  
...make completely responsive  
...finish tests  
...add DRY principles  
...refactor