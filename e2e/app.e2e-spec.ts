import { AnonyBoxPage } from './app.po';

describe('anony-box App', function() {
  let page: AnonyBoxPage;

  beforeEach(() => {
    page = new AnonyBoxPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
