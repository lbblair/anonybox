// Get dependencies
const express = require('express');
const app = module.exports = express();
const path = require('path');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const configDB = require('./server/config/database.js');

/*====== Get configurations ============*/
// Connect to our database
mongoose.connect(process.env.MONGODB_URI || configDB.url, { useMongoClient: true });
app.set('superSecret', process.env.MONGODB_SECRET || configDB.secret); // secret variable

// Set view engine
app.set('views', path.join(__dirname, './dist'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Point static path to dist
app.use(express.static(path.join(__dirname, './dist')));

// use morgan to log requests to the console
app.use(morgan('dev'));

/*====== Get routes ============*/
// Get our API routes
const index = require('./server/routes/index');
const api = require('./server/routes/api');

// Set our API routes
app.use('/api', api);
app.use('/', index);

/*====== Get server ready to launch ============*/
// Get port environment and store in express
const port = process.env.PORT || 3000;
app.set('port', port);

// Create HTTP server
const server = http.createServer(app);

// Listen on provided port, on all network interfaces
server.listen(port, () => { console.log(`Your node js server is running on port: ` + port)});
