const mongoose = require('mongoose');

const boxSchema = mongoose.Schema({
  name: String,
  details: String,
  status: { type : String , "default" : 'Open' },
  responses: { type : Array , "default" : [] },
  madeBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Box', boxSchema);
