const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const userSchema = mongoose.Schema({
  email: String,
  password: String
});

userSchema.pre('update', function(next){
  var update = this._update;
    if (update.$set && update.$set.password && update.$set.email) {
      this.update({}, {
        email: update.$set.email,
        password: bcrypt.hashSync(update.$set.password, saltRounds)
      });
    }
    next()
})

// generating a hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, saltRounds);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
