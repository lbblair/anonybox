const express = require('express');
const app = require('../../server');
const User = require('../models/user');
const Box = require('../models/box');
const jwt = require('jsonwebtoken');
const router = express.Router();

/*----------------------------------------
:Signup, Login, and Authentication Routes
----------------------------------------*/
// process the signup form
router.post('/signup', function(req, res) {
  let boxname = req.body.boxname || '';
  let email = req.body.email || '';
  let password = req.body.password || '';

  if (email == '' || password == '' || boxname == '') {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Invalid credentials"
    });
    return;
  }
  else {
    var newUser = new User();
    var newBox = new Box();
    newBox.name = boxname;
    newUser.email = email;
    newUser.password = newUser.generateHash(password);

    var joinedResponse = {};

    newUser.save(function(err, result) {
      if (err) throw err;
      // To Handle error message
      //res.status(400).send({message: err})
      joinedResponse.newUser = result;
      newBox.madeBy = newUser._id;

      newBox.save(function(err, result2){
        if(err) throw err;
        joinedResponse.newBox = result2;
        res.json({ success: true, joinedResponse });
      });
    });
  }
});

// login the users
router.post('/login', function(req, res) {
  // find the user
  User.findOne({ email: req.body.email }, function(err, user) {

    if (err)
      throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    }
    else if (user) {
      // check if password matches
      if (!user.validPassword(req.body.password)) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      }
      else {
        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, app.get('superSecret'), {
          expiresIn: '24h' // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Login was successful!',
          auth_token: token,
          email: user.email
        });
      }
    }

  });
});


/*----------------------------------------
:Data from a specific box
----------------------------------------*/
// respond with a specific box
router.get('/anonymous/boxes/:id', function(req, res) {
  Box.find({ _id: req.params.id }, function(err, box) {
    let boxForAnonymous = {
      name: box[0].name,
      details: box[0].details
    };
    res.json(boxForAnonymous);
  });
});

/*----------------------------------------
:Anonymous response to a specific box
----------------------------------------*/
router.post('/boxes/:id', function(req, res) {
  let subject = req.body.subject || '';
  let message = req.body.message || '';
  let id = req.body._id;

  if (subject == '' || message == '') {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Subject and message fields are invalid."
    });
    return;
  }
  else {
    Box.findOneAndUpdate(
      {_id: id},
      {
        $push: {
          responses: {
            subject: subject,
            message: message,
            received: new Date()
          }
        }
      },
      {upsert: true},
      function(err, box) {

        if (err)
          throw err;

        if (!box) {
          res.json({ success: false, message: 'Authentication failed. Box not found.' });
        }

        else {
          // return the information including token as JSON
          res.json({
            success: true,
            message: 'Message was submitted successfuly!',
            box: box
          });
        }
    });
  }
});

/*----------------------------------------
:Token Verification
----------------------------------------*/

// route middleware to verify a token
// place all protected routes below this and all unprotected routes above
router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, app.get('superSecret'), function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      }
      else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        req.user_id = req.decoded._doc._id;
        next();
      }
    });
  }
  else {
    // if there is no token return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
});

/*----------------------------------------
:User Routes
----------------------------------------*/

// respond with all users
router.get('/users', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});

/*----------------------------------------
:Box Routes
----------------------------------------*/

// respond with all boxes
router.get('/boxes', function(req, res) {
  Box.find({ madeBy: req.user_id }, function(err, boxes) {
    res.json(boxes);
  });
});

// respond with a specific box
router.get('/boxes/:id', function(req, res) {
  Box.find({ _id: req.params.id }, function(err, box) {
    res.json(box);
  });
});

router.post('/box/new', function(req, res) {
  let name = req.body.name || '';
  let details = req.body.details || '';
  let userId = req.user_id || '';

  if (name == '' || details == '') {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Fields are empty or invalid."
    });
    return;
  }
  else if(userId == ''){
    res.status(401);
    res.json({
      "status": 401,
      "message": "User not found."
    });
    return;
  }
  else {
    var createBox = {
        name: name,
        details: details,
        madeBy: userId
    };

    var newBox = new Box(createBox);

    newBox.save(function(err, box){
      if(err) throw err;

      res.json({
        success: true,
        message: 'Box was created successfuly!',
        box
      });
    });
  }
});

router.post('/boxes/:id/edit', function(req, res) {
  let name = req.body.name || '';
  let details = req.body.details || '';
  let status = req.body.status || '';
  let id = req.body._id;

  if (name == '' || details == '' || status == '') {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Fields are empty or invalid."
    });
    return;
  }
  else {
    Box.findOneAndUpdate(
      {_id: id},
      {
        name: name,
        details: details,
        status: status
      },
      {upsert: true},
      function(err, box) {

        if (err)
          throw err;

        if (!box) {
          res.json({ success: false, message: 'Authentication failed. Box not found.' });
        }

        else {
          // return the information including token as JSON
          res.json({
            success: true,
            message: 'Box was updated successfuly!',
            box: box
          });
        }
    });
  }
});

router.delete('/boxes/:id/delete', function(req, res) {
  Box.remove({ _id: req.body._id }, function (err, box){
    if(err)
      throw err;

    if(!box) {
      res.json({ success: false, message: 'Deletion failed! Box not found.' });
    }

    else {
      res.json({
        success: true,
        message: 'Box was deleted successfully!',
      });
    }
  });
});

/*----------------------------------------
:User Accounts
----------------------------------------*/
router.post('/user/edit', function(req, res) {
  let newEmail = req.body.newEmail || '';
  let newPassword = req.body.newPassword || '';
  let confirmPassword = req.body.confirmPassword || '';
  let id = req.user_id;

  if (newEmail == '' || newPassword == '') {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Fields are empty or invalid."
    });
    return;
  }

  if (newPassword !== confirmPassword) {
     throw new Error('password and confirm password do not match');
  }

  // user.password = newPassword;

  User.update(
    { _id: id },
    {$set:
      {
        email: newEmail,
        password: newPassword
      }
    },
    {upsert: true},
    function(err, user){
      console.log(user);
    if (err) throw err
    res.json({
      success: true,
      message: 'Account was updated successfully!',
      email: newEmail
    });
  });
});

module.exports = router;
