var express = require('express');
var router = express.Router();

/* GET Angular 2 routes. */
router.get('*', function (req, res) {
    res.render('index.html');
});

module.exports = router;
