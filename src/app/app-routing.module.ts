import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginGuard } from './guards/login.guard';
import { HomeComponent } from './components/home/home.component';
import { FaqsComponent } from './components/faqs/faqs.component';
import { LoginComponent } from './components/accounts/login/login.component';
import { SignupComponent } from './components/accounts/signup/signup.component';
import { UpdateAccountComponent } from './components/user/update/user-update.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BoxesComponent } from './components/boxes/boxes.component';
import { EditBoxComponent } from './components/boxes/edit/edit-box.component';
import { NewBoxComponent } from './components/boxes/new/new-box.component';
import { ResponseComponent } from './components/response/response.component';
import { ResponseSubmittedComponent } from './components/response/response-submitted.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'faqs',
    component: FaqsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'boxes',
    component: BoxesComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'boxes/new',
    component: NewBoxComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'boxes/:id',
    component: BoxesComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'boxes/:id/edit',
    component: EditBoxComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'boxes/:id/share',
    component: BoxesComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'user/edit',
    component: UpdateAccountComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'r/:id',
    component: ResponseComponent
  },
  {
    path: 'responses',
    component: ResponseSubmittedComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class RoutingModule { }
