import { Component } from '@angular/core';

import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private loggedIn: Boolean;

  constructor(private authService: AuthService){
    this.authService.changes.subscribe(status => this.loggedIn = status);
  }

  title = 'app works!';
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

}
