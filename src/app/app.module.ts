import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { RoutingModule } from './app-routing.module';
import { LoginGuard } from './guards/login.guard';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { BoxService } from './services/box.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header-bar/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FaqsComponent } from './components/faqs/faqs.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/accounts/login/login.component';
import { SignupComponent } from './components/accounts/signup/signup.component';
import { UpdateAccountComponent } from './components/user/update/user-update.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BoxesComponent } from './components/boxes/boxes.component';
import { EditBoxComponent } from './components/boxes/edit/edit-box.component';
import { NewBoxComponent } from './components/boxes/new/new-box.component';
import { ResponseComponent } from './components/response/response.component';
import { ResponseSubmittedComponent } from './components/response/response-submitted.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    UpdateAccountComponent,
    FaqsComponent,
    DashboardComponent,
    BoxesComponent,
    EditBoxComponent,
    NewBoxComponent,
    ResponseComponent,
    ResponseSubmittedComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthService, UserService, BoxService, LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
