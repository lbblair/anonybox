import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})

export class LoginComponent {
  constructor(private router: Router,
              private authService: AuthService,
              private toastService: ToastrService){}

  user = {};

  onSubmit(user){
    this.authService.login(user).subscribe((result) => {
      if(result.success){
        this.authService.changes.next(true);
        this.router.navigate(['dashboard']);
        this.toastService.success(result.message, 'Success!');
      }
      else {
        this.toastService.error(result.message, 'Error!');
      }
    });
  }
}
