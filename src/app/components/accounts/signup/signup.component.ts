import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html'
})

export class SignupComponent {
  constructor(private router: Router, private authService: AuthService){}

  data = {};

  onSubmit(data){
    this.authService.signup(data).subscribe((result) => {
      if(!result){
        this.router.navigate(['signup']);
      }
    });
  }
}
