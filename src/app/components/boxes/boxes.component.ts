import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

import { environment } from '../../../environments/environment';
import { BoxService } from '../../services/box.service';

@Component({
  selector: 'boxes',
  templateUrl: './boxes.component.html'
})

export class BoxesComponent implements OnInit, OnDestroy {
  public currentBox: Object;
  private boxId: String;
  private boxShare: String
  private suggestions = Array;
  private subscription: Subscription;

  constructor(private boxService: BoxService,
              private router: Router,
              private activatedRoute: ActivatedRoute){}

  title = '';
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  hasResponse = false;
  shareBox = false;
  boxUrl = '';
  responseCount = 0;
  date = '';

  ngOnInit() {
    // subscribe to router event
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        this.boxId = param['id'];
      }
    );
    this.shareLink();
    this.getCurrentBox(this.boxId);
  }

  getCurrentBox(id){
    this.boxService.getCurrentBox(id).subscribe((result) => {
      if(result){
        this.currentBox = result;
        this.title = this.currentBox[0].name;
        this.boxUrl = environment.domain + 'r/' + id;
        this.suggestions = this.currentBox[0].responses;
        this.responseCount = this.currentBox[0].responses.length;
        this.hasSuggestions(result);
      }
    });
  }

  formatDate(result){
    this.date = moment(result.received).format('ddd, MMMM D, YYYY [at] h:mm A');
    return this.date;
  }

  hasSuggestions(result){
    for(var i = 0; i < result.length; i++){
      if(result[i].responses.length > 0) this.hasResponse = true;
    }
  }

  shareLink(){
    if(window.location.href.indexOf('share') > -1){
      this.shareBox = !this.shareBox;
    }
  }

  ngOnDestroy() {
    // prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }
}
