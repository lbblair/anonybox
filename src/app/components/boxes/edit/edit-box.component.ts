import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

import { BoxService } from '../../../services/box.service';

@Component({
  selector: 'edit-box',
  templateUrl: './edit-box.component.html'
})

export class EditBoxComponent implements OnInit {
  private subscription: Subscription;
  private boxId: String;
  public editCurrentBox: Object;
  public box: Object;

  constructor(private activatedRoute: ActivatedRoute,
              private boxService: BoxService,
              private router: Router,
              private toastService: ToastrService){}

  boxTitle = '';
  boxDetails = '';
  radioValues = [
    {value: 'Open', display: 'Open'},
    {value: 'Closed', display: 'Closed'}
  ];
  status: '';
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

  ngOnInit() {
    // subscribe to router event
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        this.boxId = param['id'];
      }
    );
    this.getEditableBox(this.boxId);
  }

  getEditableBox(id){
    this.boxService.getCurrentBox(id).subscribe((result) => {
      if(result){
        this.editCurrentBox = result;
        this.boxTitle = this.editCurrentBox[0].name;
        this.boxDetails = this.editCurrentBox[0].details;
        this.status = this.editCurrentBox[0].status;
      }
    });
  }

  onSubmit(formData){
    formData._id = this.boxId;
    formData.status = formData.item;
    for(var k in formData) {
      if(k === 'item'){
        delete(formData[k]);
      }
    }
    this.boxService.editBox(formData).subscribe((result) => {
      if(result.success){
        this.router.navigate(['dashboard']);
        this.toastService.success(result.message, 'Success!');
      }
      else {
        this.toastService.error(result.message, 'Error!');
      }
    });
  }

  cancelEdit(form){
    form.reset();
    this.router.navigate(['dashboard']);
  }

}
