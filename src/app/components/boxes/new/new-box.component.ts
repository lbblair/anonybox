import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

import { BoxService } from '../../../services/box.service';

@Component({
  selector: 'new-box',
  templateUrl: '../new/new-box.component.html'
})

export class NewBoxComponent {

  constructor(private router: Router,
              private boxService: BoxService,
              private toastService: ToastrService){}

  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  boxTitle = '';
  boxDetails = '';

  onSubmit(formData){
    this.boxService.createBox(formData).subscribe((result) => {
      if(result.success){
        this.router.navigate(['dashboard']);
        this.toastService.success(result.message, 'Success!');
      }
      else {
        this.toastService.error(result.message, 'Error!');
      }
    });
  }


  cancelNewBox(form){
    form.reset();
    this.router.navigate(['dashboard']);
  }

}
