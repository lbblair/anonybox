import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

import { SidebarComponent } from '../sidebar/sidebar.component';
import { AuthService } from '../../services/auth.service';
import { BoxService } from '../../services/box.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {
  public boxes: Object;
  public boxClosed: Boolean;
  public boxOpen: Boolean;
  private latestResponseTime: Boolean;

  constructor(private router: Router,
              private authService: AuthService,
              private boxService: BoxService,
              private toastService: ToastrService){}

  title = 'My Anonymous Boxes';
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  hasBoxes = true;

  ngOnInit(){
    this.boxService.getBoxes().subscribe((result) => {
      if(result.length > 0){
        this.boxes = result;
        for(var i = 0; i < result.length; i++){
          this.checkStatus(this.boxes[i].status);
        }
      }
      else {
        this.hasBoxes = !this.hasBoxes;
      }
    });
  }

  checkStatus(status){
    this.boxOpen = status == 'Open' ? true : false;
    this.boxClosed = status == 'Closed' ? true : false;
  }

  getLatestResponse(box){
    let idx = box.responses.length - 1;
    box.timeStamp = box.responses[idx].received;
    this.latestResponseTime = box.timeStamp ? true : false;
    return moment(box.timeStamp).format('ddd, MMMM D, YYYY [at] h:mm A');
  }

  ifResponse(box){
    this.latestResponseTime = box.responses.length ? true : false;
    return this.latestResponseTime;
  }

  deleteBox(boxId: any, index: number){
    this.boxService.deleteSelectedBox(boxId).subscribe((result) => {
      if(result.success){
        this.ngOnInit();

        if(index < 1) {
          this.toastService.success(result.message, 'Success!');
          this.router.navigate(['boxes/new']);
        }
      }

      else{
        this.toastService.error(result.message, 'Error!');
      }
    });
  }

  // TODO: Remove this later
  logout(){
    this.authService.logout();
  }
}
