import { Component } from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html'
})

export class FooterComponent {
  constructor(){}
  d = new Date()
  copyrightDate = this.d.getFullYear();
}
