import { Component } from '@angular/core';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'header-bar',
  templateUrl: './header.component.html'
})

export class HeaderComponent {
  constructor(private authService: AuthService){}

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }
}
