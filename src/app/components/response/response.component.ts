import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { environment } from '../../../environments/environment';
import { BoxService } from '../../services/box.service';

@Component({
  selector: 'box-response',
  templateUrl: './response.component.html',
})

export class ResponseComponent implements OnInit, OnDestroy  {
  private subscription: Subscription;
  private boxId: String;
  suggestion = {};
  anonyBox: Object = {};
  appDomain: String;

  constructor(private router: Router,
              private boxService: BoxService,
              private activatedRoute: ActivatedRoute,
              private toastService: ToastrService){
    this.appDomain = environment.domain;
  }

  ngOnInit(){
    // subscribe to router event
    this.subscription = this.activatedRoute.params.subscribe(
      (param: any) => {
        this.boxId = param['id'];
      }
    );

    this.boxService.getAnonymousBox(this.boxId).subscribe((result) => {
      if(result){
        this.anonyBox = result;
      }
    });
  }

  onSubmit(suggestion){
    suggestion._id = this.boxId;
    if (!suggestion.subject) {
      suggestion.subject = 'No subject provided.'
    }
    this.boxService.boxResponse(suggestion).subscribe((result) => {
      if(result.success){
        // TODO: Route this somewhere else
        this.router.navigate(['responses']);
        this.toastService.success(result.message, 'Success!');
      }
    });
  }

  ngOnDestroy(){}
}
