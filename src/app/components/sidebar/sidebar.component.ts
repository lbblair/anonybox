import { Component } from '@angular/core';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'main-sidebar',
  templateUrl: './sidebar.component.html'
})

export class SidebarComponent {

  constructor(private authService: AuthService){}

  signOut(){
    this.authService.changes.next(false);
    this.authService.logout();
  }
}
