import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

import { UserService } from '../../../services/user.service';

@Component({
  selector: 'user-update',
  templateUrl: './user-update.component.html',
})

export class UpdateAccountComponent implements OnInit {
  constructor(private userService: UserService,
              private toastService: ToastrService,
              private router: Router){}

  title = 'Edit Account';
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  user = {newEmail: '', newPassword: '', confirmPassword: ''};

  ngOnInit(){
    this.user.newEmail = this.currentUser.email;
  }

  onSubmit(formData){
    this.userService.editUser(formData).subscribe((result) => {
      if(result.success){
        sessionStorage.setItem('currentUser', JSON.stringify({token: this.currentUser.token, email: result.email}));
        this.router.navigate(['dashboard']);
        this.toastService.success(result.message, 'Success!');
      }
      else {
        this.toastService.error(result.message, 'Error!');
      }
    });
  }

  cancelEdit(form){
    form.reset();
    this.router.navigate(['dashboard']);
  }

}
