import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from "rxjs";

import { AuthService } from '../services/auth.service';

@Injectable()
export class LoginGuard implements CanActivate {

  private isLoggedIn: boolean;

  constructor(private user: AuthService, private router: Router) {}

  canActivate() {
    if(!this.user.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
    return this.user.isLoggedIn();
  }
}
