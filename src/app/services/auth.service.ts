import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

// Import RxJs required methods
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  private loggedIn = false;
  public token: string;
  public changes = new BehaviorSubject(false);

  constructor(private http: Http,
              private router: Router,
              private toastService: ToastrService){
    var currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
    this.loggedIn = !!sessionStorage.getItem('currentUser');
  }
  private apiUrl = environment.apiURL;

  // signup if no account
  signup(data): Observable<any> {
    let headers = new Headers();
    let params = JSON.stringify(data);
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'signup', params, {headers})
      .map(res => res.json())
      .map((res) => {
        if (res.success) {
          this.login(data).subscribe((result) => {
            if(result){
              let id = res.joinedResponse.newBox._id;
              let msg = 'Box was created succesfully.'
              this.changes.next(true);
              this.router.navigate(['boxes/' + id + '/edit']);
              this.toastService.success(msg, 'Success!');
            }
          });
        }
        else {
          // Todo: Handle this!!!!
          return !res.success;
        }
      });
  }

  // log user in
  login(user) {
    let headers = new Headers();
    let params = JSON.stringify({email: user.email, password: user.password});
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'login', params, {headers})
      .map(res => res.json())
      .map((res) => {
        if (res.success && res.auth_token) {
         this.token = res.auth_token;
         sessionStorage.setItem('currentUser', JSON.stringify({token: res.auth_token, email: res.email}));
         this.loggedIn = true;
        }
        return res;
      });
  }

  // log user out
  logout() {
    sessionStorage.clear();
    this.loggedIn = false;
    this.router.navigate(['login']);
  }

  // check if user is logged in
  isLoggedIn() {
    return this.loggedIn;
  }
}
