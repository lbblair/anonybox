import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
// Import RxJs required methods
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';

@Injectable()
export class BoxService {

  constructor(private http: Http, private authService: AuthService){}

  private apiUrl = environment.apiURL;

  getBoxes(): Observable<any> {
    // add authorization header with jwt token
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers });

    // get boxes from api
    return this.http.get(this.apiUrl + 'boxes', options).map(res => res.json());
  }

  getAnonymousBox(id): Observable<any> {
    let headers = new Headers();
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.apiUrl + 'anonymous/boxes/' + id, options).map(res => res.json());
  }

  getCurrentBox(id): Observable<any> {
    // add authorization header with jwt token
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers });

    // get box from api
    return this.http.get(this.apiUrl + 'boxes/' + id, options).map(res => res.json());
  }

  createBox(data): Observable<any> {
    // add authorization header with jwt token
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let params = JSON.stringify(data);
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'box/new', params, {headers})
      .map(res => res.json())
      .map((res) => {
        if (res) return res;
      });
  }

  editBox(data): Observable<any> {
    // add authorization header with jwt token
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let params = JSON.stringify(data);
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'boxes/' + data._id + '/edit' , params, {headers})
      .map(res => res.json())
      .map((res) => {
        if (res) return res;
      });
  }

  deleteSelectedBox(id: number) {
    let headers = new Headers({ 'x-access-token': this.authService.token });
    headers.append('Content-Type', 'application/json');
    let data = {_id: id};
    let options = new RequestOptions({headers: headers, body: data});
    let apiUrl = this.apiUrl + 'boxes/' + id + '/delete';

    // delete() method only accepts url, options params,
    // so need to do something like above to pass data
    return this.http
      .delete(apiUrl, options)
      .map(res => res.json())
      .map((res) => {
        if(res) return res;
      })
  }

  boxResponse(data): Observable<any> {
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let params = JSON.stringify(data);
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'boxes/' + data._id, params, {headers})
      .map(res => res.json())
      .map((res) => {
        if (res) return res;
      });
  }
}
