import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
// Import RxJs required methods
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

@Injectable()
export class UserService {

  constructor(private http: Http){}

  private apiUrl = environment.apiURL;
  currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

  editUser(data): Observable<any> {
    // add authorization header with jwt token
    let headers = new Headers({ 'x-access-token': this.currentUser.token });
    let params = JSON.stringify(data);
    headers.append('Content-Type', 'application/json');

    return this.http
      .post(this.apiUrl + 'user/edit' , params, {headers})
      .map(res => res.json())
      .map((res) => {
        console.log(res);
        if (res) return res;
      });
  }

}
