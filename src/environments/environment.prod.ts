export const environment = {
  production: true,
  apiURL: 'https://dev-anonybox.herokuapp.com/api/',
  domain: 'https://dev-anonybox.herokuapp.com/'
};
