const test = require('ava');
const request = require('supertest');
const app = require('../../server');

test('POST /signup', function (assert) {
  var newData = { email: 'test1@test.com', password: '1234', boxname: 'New Box'};
  request(app)
    .post('/api/signup')
    .send(newData)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function (err, res) {
      var actualData = [res.body.joinedResponse.newUser.email, res.body.joinedResponse.newBox.name];
      var expectedData = [newData.email, newData.boxname];
      assert.truthy(err, err);
      assert.is(actualData, expectedData, {success: true});
    });
});

test('POST /login', function (assert) {
  var loginInfo = { email: 'test1@test.com', password: '1234'};
  request(app)
    .post('/api/login')
    .send(loginInfo)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function (err, res) {
      var actualLogin = res.body.email;
      var expectedLogin = loginInfo.email;
      assert.ifError(err, err);
      assert.is(actualLogin, expectedLogin, {success: true});
    });
});

test('GET /boxes', function (assert) {
  request(app)
    .get('/api/boxes')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function (err, res) {

      var expectedBox = [{
        id: 2,
        name: 'New Box',
        details: 'Make a suggestion',
        responses: [],
        madeBy: 1
      }];

      var actualBox = res.body;

      assert.false(actualBox.success, 'No token provided.');
      // assert.ifError(err, err);
      // assert.is(actualBox, expectedBox, {});
    });
});
